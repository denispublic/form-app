import React from 'react';
import classes from './App.css';
import Form from './containers/Form';

function App() {
  return (
    <div className={classes.App}>
      <Form />
    </div>
  );
}

export default App;

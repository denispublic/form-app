import React from 'react';
import Auxiliary from '../hoc/Auxiliary';
import classes from './input.css';

const input = (props) => {
    let inputField =  null;
    let inputStatus = classes.Default + ' ' + classes.Input;
    /*
    * Add invalid css property to to input field, if it is invalid
    */
    if(props.validation && props.invalid && props.touched) {
        inputStatus = classes.Invalid  + ' ' + classes.Input;
    }

    switch (props.type) {
        case ("input"):
            inputField = <input {...props.config} 
                    onChange={props.changed}
                    className={inputStatus}/>;
            break;
        case ("textarea"):
            inputField = <textarea {...props.config} 
            onChange={props.changed}
            className={inputStatus}/>;
            break;
        case ("select"):
            inputField=<select onChange={props.changed}
            className={inputStatus}>
                    {props.config.options.map(option => 
                        <option key={option.value} value={option.value}>
                            {option.displayValue}
                        </option>
                    )}
                </select>
            break;
        default:
            inputField = <input {...props.config} 
            onChange={props.changed}
            className={inputStatus} />;
    }

    return (
        <div>
            {inputField}
        </div>
    );
};

export default input;
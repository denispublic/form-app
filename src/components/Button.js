import React from 'react';
import classes from './Button.css'

const Button = (props) => {
    let btnClass = props.isDisabled ? classes.BtnDanger : classes.BtnSuccess;
    return (
        <button disabled={props.isDisabled}
                type={props.btnType}
                className={classes.Default + ' ' + btnClass}>
                    {props.btnTitle}
                </button>
    );
};

export default Button;
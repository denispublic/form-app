import React, { Component } from 'react'
import Button from '../components/Button';
import Input from '../components/Input';
import Auxiliary from '../hoc/Auxiliary';
import classes from './Form.css';

class Form extends Component {
    /*
    * Make sure to follow json structure
    * to properly render the form
    */
    state = {
        controls: {
            username: {
                elementType: "input",
                elementConfig: {
                    type: "text",
                    placeholder: "Username"
                },
                value: '',
                validation: {
                    required: true,
                },
                valid: false,
                touched: false
            },
            gender: {
                elementType: "select",
                elementConfig:{
                    options: [
                        {value: 'male', displayValue: 'Male'},
                        {value: 'female', displayValue: 'Female'}
                    ]
                },
            validation: {
                required: true
            },
            value: ''
            },
            email: {
                elementType: "input",
                elementConfig: {
                    type: "email",
                    placeholder: "Email address"
                },
                value: '',
                validation: {
                    required: true,
                    isEmail: true,
                },
                valid: false
            },
            password: {
                elementType: "input",
                elementConfig: {
                    type: "password",
                    placeholder: "Password"
                },
                value: '',
                validation: {
                    required: true,
                    isPassword: true
                },
                valid: false
            },
            repeatPassword: {
                elementType: "input",
                elementConfig: {
                    type: "password",
                    placeholder: "Repeat password"
                },
                value: '',
                validation: {
                    required: true,
                    isPassword: true,
                    matchesPassword: true
                },
                valid: false
            },
            phoneNumber: {
                elementType: "input",
                elementConfig: {
                    type: "tel",
                    placeholder: "Phone Number"
                },
                value: '',
                validation: {
                    required: true,
                    isPhone: true
                },
                valid: false
            },
        },
        validForm: false
    }

    validationCheck = ( value, rules ) => {
        let isValid = true;

        // end validation if there are no rules
        if( !rules ) {
            return true;
        }

        // check if required field is empty
        if( rules.required ) {
            isValid = value.trim() !== '';
        }

        /* 
        * check if email matches the pattern
        * feel free to change the pattern
        */
        if( rules.isEmail ) {
            const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
            isValid = pattern.test(value);
        }

        /* 
        * check if phone is numeric
        * feel free to change the pattern
        */
        if( rules.isPhone ) {
            const pattern = /^\d+$/;
            isValid = pattern.test(value);
        }

        /*
        * check a password between 6 to 20 characters 
        * which contain at least one numeric digit, 
        * one uppercase and one lowercase letter
        * feel free to change the pattern
        */
        if( rules.isPassword ) {
            const pattern = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;
            isValid = pattern.test(value);
        }

        /*
        * only check for passwords match if password exists
        */
        if( rules.matchesPassword ) {
            const password = this.state.controls.password.value;
            const passwordIsValid = this.state.controls.password.valid;
                isValid = passwordIsValid && password === value;
        }

        return isValid;
    };

    inputChangedHandler = (event, elementToUpdate) => {
        const updatedForm = {
            ...this.state.controls
        };
        const updatedFormElement = {
            ...updatedForm[elementToUpdate]
        };
        
        updatedFormElement.value = event.target.value;

        updatedFormElement.valid = this.validationCheck(updatedFormElement.value, updatedFormElement.validation)
        
        updatedFormElement.touched = true;

        updatedForm[elementToUpdate] = updatedFormElement;

        this.setState({controls: updatedForm});
        
        /*
        * After updating the state, check to see if the form is valid
        */
        let formIsValid;
        for (let key in this.state.controls) {
            this.state.controls[key].valid ? formIsValid = true : formIsValid = false;
        }
        this.setState({validForm: formIsValid})
    };

    formSubmitHandler = (e) => {
        e.preventDefault();
    }

    render() {

        const formElementsArray = [];
        for (let key in this.state.controls) {
            formElementsArray.push({
                id: key,
                config: this.state.controls[key]
            })
        };
        let form = (
            <form onSubmit={this.formSubmitHandler} className={classes.Form}>
                {formElementsArray.map(item => (
                    <Input  key={item.id}
                            type={item.config.elementType}
                            config={item.config.elementConfig}
                            value={item.config.value} 
                            changed={(event) => this.inputChangedHandler(event, item.id)}
                            invalid={!item.config.valid}
                            touched={item.config.touched}
                            validation={item.config.validation.required}/>
                ))}
                <Button btnType="submit" 
                        isDisabled={!this.state.validForm}
                        btnTitle="SUBMIT"></Button>
            </form>
        );
        
        return (
            <Auxiliary>
                <h1 className={classes.Heading}>Submit the form bellow</h1>
                {form}
            </Auxiliary>          
        )
    }
}

export default Form;
